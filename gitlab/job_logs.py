from logging import info, warning

from api import gitlab
from utilities import types, validate

gitlab = gitlab.GitLab(types.Arguments().url)
args = types.Arguments()


def get_all(project_id, project_url):
    job_logs = []
    info("[*] Retrieving job stats for project %s", project_url)
    jobs = gitlab.get_jobs(project_id)
    if validate.api_result(jobs):
        warning("[*] Found %s jobs for project %s", len(jobs), project_url)
        for job in jobs:
            info("[*] Retrieving job %s, which completed at %s", job['id'], job['finished_at'])
            job_log = gitlab.get_job_logs(project_id, job['id'])
            job_logs.append(types.JobLog(job['id'], job['web_url'], job_log))
    return job_logs


def sniff_secrets(job_log):
    monitor = types.SecretsMonitor()
    return monitor.sniff_secrets({job_log.web_url: job_log.trace})
